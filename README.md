# Installing a new environment using Flux v2

1. Create `cluster-foundations` namespace
2. Clone `kbc-release` repository
3. Create `kbc-config` repository
4. Pull `cluster-initializer` OCI image
5. Create `values.yaml` file
6. Use the `values.yaml` file to deploy the initial environment

## Create `cluster-foundations` namespace

```bash
kubectl create ns cluster-foundations
```

## Create `kbc-config` repository

Create a kbc-config repository on your git server. This repository will reference kbc-release. In this repository we can add cluster specific configuration, patches, ...

## Pull `cluster-initializer` OCI image

```bash
docker pull harbor.sofico.be/ops/miles-cluster-initializer-v2:1.2.0
```

## Create `values.yaml` file

Create a custom values.yaml file, containing environment specific variables needed to run `flux-v2`.
To create this file, start from the template, and follow these steps to fill out the file:

1. Add necessary helm-repo's to the file:

2. Base64 encode ssh-private-key, known-hosts and ssh-public-key:

Generate a new ssh key (optional):

```bash
ssh-keygen
```

Base64 encode known_hosts:
```bash
ssh-keyscan example-server.com | base64 | tr -d "\n"
```

Base64 encode public key:
```bash
cat /home/user/.ssh/id_rsa.pub | base64 | tr -d "\n"
```

Base64 encode private key:
```bash
cat /home/user/.ssh/id_rsa | base64 | tr -d "\n"
```

Paste the results in the corresponding fields of the template.

## Use the `values.yaml` file to deploy the initial environment

This command will create a helm release for Flux's controllers, amongst other resources defined in the values.yaml file.

```bash
docker run -it -v $PWD/values.yaml:/tmp/values.yaml -v ~/CHANGEME:/root/.kube/config -e FLUX_VALUES_FILE='/tmp/values.yaml' -e FLUX_NAMESPACE=cluster-foundations harbor.sofico.be/ops/miles-cluster-initializer-v2:1.3.0
```